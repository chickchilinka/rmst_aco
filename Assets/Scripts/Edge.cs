﻿public class Edge
{
    public Vertex start;
    public Vertex end;
    public double intensity;
    public double length {
        get {

            return start.Distance(end);
        }
    }
    public Edge(Vertex start, Vertex end) {
        this.start = start;
        this.end = end;
    }

    public void updateIntensity(double length) {
        double deltaTau;
        if (length == 0) {
            deltaTau = 0;
        }
        else {
            deltaTau = AntColony.Q / length;
        }
        intensity=(1 - AntColony.rho) * intensity + AntColony.rho * deltaTau;
    }
}
