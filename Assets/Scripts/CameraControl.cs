﻿using UnityEngine;
public class CameraControl : MonoBehaviour
{
    public float Sensitivity = 1;
    public float ScaleSensitivity = 1;

    void Update() {
        transform.position += new Vector3(Input.GetAxis("Horizontal") * Time.deltaTime * Sensitivity, Input.GetAxis("Vertical") * Time.deltaTime * Sensitivity, 0);
        GetComponent<Camera>().orthographicSize -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * ScaleSensitivity;
        GetComponent<Camera>().orthographicSize = Mathf.Clamp(GetComponent<Camera>().orthographicSize, 70, 400);
    }
}
