﻿using System.Collections.Generic;
using System;
public class AntColony
{
    public static double Q = 1000;
    public static double rho = 0.1d;
    public static double gamma = 1;
    public static double alpha = 1;
    public static double beta = 1;
    private List<Ant> ants;
    public AntColony(List<Vertex> terminals) {
        ants = new List<Ant>();
        int id = 0;
        foreach (var terminal in terminals) {
            ants.Add(new Ant(terminal));
            id++;
        }
    }
    public int antNumber {
        get {
            int sum = 0;
            foreach (var ant in ants) {
                sum += ant.isAlive ? 1 : 0;
            }
            return sum;
        }
    }
    public AntMoveResult AntMove(Ant ant, GraphGrid grid) {
        AntMoveResult result = new AntMoveResult();
        Dictionary<Vertex, double> probablities = getProbabilities(ant, grid);
        if (probablities.Count == 0) {
            result.confused = true;
        }
        else {
            double max = double.MinValue;
            Vertex choice = null;
            foreach (var vert in probablities.Keys) {
                if (probablities[vert] > max) {
                    max = probablities[vert];
                    choice = vert;
                }
            }
            ant.currentVertex = getNearestPathPoint(choice, ant.tabuList);
            result = ConstructWay(ant.currentVertex, choice, grid, ant);
            ant.currentVertex = choice;
        }
        return result;
    }

    private AntMoveResult ConstructWay(Vertex start, Vertex end, GraphGrid grid, Ant ant) {
        List<Vertex> tabu = new List<Vertex>();
        List<Edge> path = new List<Edge>();
        AntMoveResult result = new AntMoveResult();
        if (start.x == end.x) {
            path.AddRange(FindEdgesBetween(start, end, grid, false, out tabu));
        }
        else if (start.y == end.y) {
            path.AddRange(FindEdgesBetween(start, end, grid, true, out tabu));
        }
        else {
            var cornerVertex = grid.gridVerticesTabled[start.column, end.row];
            List<Vertex> tabu1;
            List<Vertex> tabu2;
            if (cornerVertex.x == start.x) {
                path.AddRange(FindEdgesBetween(start, cornerVertex, grid, false, out tabu1));
                path.AddRange(FindEdgesBetween(cornerVertex, end, grid, true, out tabu2));
            }
            else {
                path.AddRange(FindEdgesBetween(start, cornerVertex, grid, true, out tabu1));
                path.AddRange(FindEdgesBetween(cornerVertex, end, grid, false, out tabu2));
            }
            tabu.AddRange(tabu1);
            tabu.AddRange(tabu2);
        }
        List<Vertex> metVerts = new List<Vertex>();
        foreach (var vert in tabu) {
            bool met = isMetAntOnPoint(ant, vert, out result.metAnt);
            metVerts.Add(vert);
            result.isMetAnt = met | result.isMetAnt;
        }
        ant.tabuList.AddRange(metVerts);
        ant.path.AddRange(path);
        return result;

    }

    private double intensityOfPath(Vertex start, Vertex end, Ant ant, GraphGrid grid) {
        if (start.x == end.x) {
            List<Vertex> tabu;
            double sumIntensity = 0;
            int count = 0;
            foreach (var edge in FindEdgesBetween(start, end, grid, false, out tabu)) {
                count++;
                sumIntensity += edge.intensity;
            }
            sumIntensity = sumIntensity / count;
            return sumIntensity;
        }
        else if (start.y == end.y) {
            List<Vertex> tabu;
            double sumIntensity = 0;
            int count = 0;
            foreach (var edge in FindEdgesBetween(start, end, grid, true, out tabu)) {
                count++;
                sumIntensity += edge.intensity;
            }
            sumIntensity = sumIntensity / count;
            return sumIntensity;
        }
        else {
            var cornerVertex = grid.gridVerticesTabled[start.column, end.row];
            List<Vertex> tabu1;
            if (cornerVertex.x == start.x) {
                double sumIntensity = 0;
                int count = 0;
                foreach (var edge in FindEdgesBetween(start, cornerVertex, grid, false, out tabu1)) {
                    count++;
                    sumIntensity += edge.intensity;
                }
                foreach (var edge in FindEdgesBetween(cornerVertex, end, grid, true, out tabu1)) {
                    count++;
                    sumIntensity += edge.intensity;
                }
                sumIntensity = sumIntensity / count;
                return sumIntensity;
            }
            else {
                double sumIntensity = 0;
                int count = 0;
                foreach (var edge in FindEdgesBetween(start, cornerVertex, grid, true, out tabu1)) {
                    count++;
                    sumIntensity += edge.intensity;
                }
                foreach (var edge in FindEdgesBetween(cornerVertex, end, grid, false, out tabu1)) {
                    count++;
                    sumIntensity += edge.intensity;
                }
                sumIntensity = sumIntensity / count;
                return sumIntensity;
            }
        }
    }
    private List<Edge> FindEdgesBetween(Vertex start, Vertex end, GraphGrid grid, bool horiz, out List<Vertex> vertices) {
        List<Edge> edges = new List<Edge>();
        vertices = new List<Vertex>();
        if (horiz) {
            int column = start.column;
            Vertex edgeStart = start;
            while (column != end.column) {
                int oldColumn = column;
                if (edgeStart.x < end.x) {
                    column++;
                }
                else {
                    column--;
                }
                edges.Add(grid[grid.gridVerticesTabled[oldColumn, edgeStart.row], grid.gridVerticesTabled[column, edgeStart.row]]);
                edgeStart = grid.gridVerticesTabled[column, edgeStart.row];
                vertices.Add(edgeStart);
            }
        }
        else {
            int row = start.row;
            Vertex edgeStart = start;
            while (row != end.row) {
                int oldRow = row;
                if (edgeStart.y < end.y) {
                    row++;
                }
                else {
                    row--;
                }
                edges.Add(grid[grid.gridVerticesTabled[edgeStart.column, oldRow], grid.gridVerticesTabled[edgeStart.column, row]]);
                edgeStart = grid.gridVerticesTabled[edgeStart.column, row];
                vertices.Add(edgeStart);
            }
        }
        return edges;
    }
    private bool isMetAntOnPoint(Ant ant, Vertex vertex, out List<Ant> metAnt) {
        bool met = false;
        metAnt = new List<Ant>();
        foreach (var a in ants) {
            if (a != ant) {
                if (a.tabuList.Contains(vertex) || a.currentVertex == vertex) {
                    metAnt.Add(a);
                    met = true;
                }
            }
        }

        return met;
    }

    public Vertex getNearestPathPoint(Vertex vert, List<Vertex> path) {
        Vertex nearest = null;
        double min = double.MaxValue;
        foreach (var v in path) {
            if (v.Distance(vert) < min) {
                min = v.Distance(vert);
                nearest = v;
            }
        }
        return nearest;
    }

    private double selectionProb(Vertex vertex, Ant ant, GraphGrid grid) {
        double dt = 0;
        if (ant.getTourLength() != 0) {
            double c = ant.getTourLength();
            dt = Q / c;
        }
        Vertex nearestInPath = getNearestPathPoint(vertex, ant.tabuList);
        double intensity = (1d - rho) * intensityOfPath(nearestInPath, vertex, ant, grid) + rho * dt;
        double psi = createPSI(ant, nearestInPath);
        double desirability = 1d / (((double)nearestInPath.Distance(vertex)) + gamma * psi);
        double intenseDesire = Math.Pow(intensity, alpha) * Math.Pow(desirability, beta);
        return intenseDesire;
    }
    private double createPSI(Ant ant, Vertex vertex) {
        double min = double.MaxValue;
        for (int antId = 0; antId < ants.Count; antId++) {
            if (ants[antId].isAlive && ants[antId] != ant) {
                foreach (var vert in ants[antId].tabuList) {
                    if (vertex.Distance(vert) < min) {
                        min = vertex.Distance(vert);
                    }
                }
            }
        }
        return min;
    }

    public List<Ant> getAnts() {
        return ants;
    }

    private Dictionary<Vertex, double> getProbabilities(Ant selectedAnt, GraphGrid grid) {
        Dictionary<Vertex, double> toReturn = new Dictionary<Vertex, double>();
        foreach (var ant in ants) {
            foreach (var vert in ant.tabuList) {
                if (!selectedAnt.tabuList.Contains(vert) && !toReturn.ContainsKey(vert)) {
                    toReturn.Add(vert, selectionProb(vert, selectedAnt, grid));
                }
            }
        }
        return toReturn;
    }
}
