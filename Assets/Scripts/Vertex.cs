﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[System.Serializable]
public class Vertex
{
    public double x;
    public double y;
    public bool isTerminal;
    public int row;
    public int column;
    public Vertex(double x, double y, bool isTerminal = false) {
        this.x = x;
        this.y = y;
        this.isTerminal = isTerminal;
    }
    public double Distance(Vertex to) {
        return Math.Abs(x - to.x) + Math.Abs(y - to.y);
    }

    public List<Edge> getIncidentEdges(Graph graph) {
        List<Edge> incident = new List<Edge>();
        foreach (var edge in graph.edges) {
            if (edge.start == this || edge.end == this) {
                incident.Add(edge);
            }
        }
        return incident;
    }
}
