﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class GraphGrid : Graph
{
    public List<Vertex> terminals;
    public List<Vertex> steinerVerts;
    public Vertex[,] gridVerticesTabled;
    public static GraphGrid buildGrid(List<Vertex> terminals) {
        GraphGrid newGrid = new GraphGrid();
        newGrid.terminals = terminals;
        newGrid.vertices = new List<Vertex>();
        List<double> horizontalCoordinates = new List<double>();
        List<double> verticalCoordinates = new List<double>();
        foreach (var vertex in terminals) {
            newGrid.vertices.Add(vertex);
            if (!horizontalCoordinates.Contains(vertex.x))
                horizontalCoordinates.Add(vertex.x);
            if (!verticalCoordinates.Contains(vertex.y))
                verticalCoordinates.Add(vertex.y);
        }
        newGrid.gridVerticesTabled = new Vertex[horizontalCoordinates.Count, verticalCoordinates.Count];
        horizontalCoordinates.Sort();
        verticalCoordinates.Sort();
        for (int i = 0; i < horizontalCoordinates.Count; i++) {
            for (int j = 0; j < verticalCoordinates.Count; j++) {
                if (newGrid[horizontalCoordinates[i], verticalCoordinates[j]] == null) {
                    newGrid[horizontalCoordinates[i], verticalCoordinates[j]] = new Vertex(horizontalCoordinates[i], verticalCoordinates[j]);
                }
                newGrid.gridVerticesTabled[i, j] = newGrid[horizontalCoordinates[i], verticalCoordinates[j]];
                newGrid[horizontalCoordinates[i], verticalCoordinates[j]].row = j;
                newGrid[horizontalCoordinates[i], verticalCoordinates[j]].column = i;
            }
        }
        for (int i = 0; i < newGrid.gridVerticesTabled.GetUpperBound(0) + 1; i++) {
            for (int j = 0; j < newGrid.gridVerticesTabled.GetUpperBound(1) + 1; j++) {
                if (j < newGrid.gridVerticesTabled.GetUpperBound(1)) {
                    newGrid.edges.Add(new Edge(newGrid.gridVerticesTabled[i, j], newGrid.gridVerticesTabled[i, j + 1]));
                }
                if (i < newGrid.gridVerticesTabled.GetUpperBound(0)) {
                    newGrid.edges.Add(new Edge(newGrid.gridVerticesTabled[i, j], newGrid.gridVerticesTabled[i + 1, j]));
                }
            }
        }
        return newGrid;
    }
    private Vertex this[double x, double y] {
        get {
            foreach (var vert in vertices) {
                if (vert.x == x && vert.y == y) {
                    return vert;
                }
            }
            return null;
        }
        set {
            if (this[x, y] == null) {
                vertices.Add(value);
            }
            else {
                for (int i = 0; i < vertices.Count; i++) {
                    if (vertices[i].x == x && vertices[i].y == y) {
                        vertices[i] = value;
                        break;
                    }
                }
            }
        }
    }
}
