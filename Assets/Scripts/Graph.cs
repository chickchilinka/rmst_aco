﻿using System.Collections.Generic;
public class Graph
{
    public List<Vertex> vertices = new List<Vertex>();
    public List<Edge> edges = new List<Edge>();

    public double getLength() {
        double sum = 0;
        for (int i = 0; i < edges.Count; i++) {
            sum += edges[i].length;
        }
        return sum;
    }
    public Edge this[Vertex i, Vertex j] {
        get {
            foreach (var edge in edges) {
                if ((edge.start == i && edge.end == j) || (edge.start == j && edge.end == i)) {
                    return edge;
                }
            }
            return null;
        }
    }
}