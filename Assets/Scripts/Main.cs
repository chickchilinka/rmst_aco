﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using SFB;
public class Main : MonoBehaviour
{
    private Graph bestSteinerTree;
    private List<Vertex> vertices;
    private GraphGrid grid;
    private double bestLength;
    private int M = 10;
    private int maxX;
    private int maxY;
    private int Maxloop = 5;
    private double inititalPheromone = 1000;
    [Space]
    public GameObject Circle;
    public GameObject Cross;
    public GameObject Edge;
    public Sprite CircleFilled;
    public List<GameObject> Circles;
    public List<GameObject> Crosses;
    public Dictionary<Edge, GameObject> Edges;
    public GameObject canvas;
    public InputField XBounds;
    public InputField YBounds;
    public InputField TerminalsCount;
    public InputField InitialIntensity;
    public InputField maxCycles;
    public InputField Rho;
    public InputField QValue;
    public InputField alpha;
    public InputField beta;
    public InputField gamma;
    public Text generation;
    public Text lengthtext;
    public Text timeText;
    public Transform graphParent;
    double time = 0;
    private void Start() {
        XBounds.text = "50";
        YBounds.text = "50";
        TerminalsCount.text = "10";
        InitialIntensity.text = "1000";
        Rho.text = "0,1";
        maxCycles.text = "5";
        QValue.text = "1000";
        alpha.text = "1";
        beta.text = "1";
        gamma.text = "1";
    }

    public void HideOrShow() {
        if (canvas.activeInHierarchy) {
            canvas.SetActive(false);
        }
        else {
            canvas.SetActive(true);
        }
    }
    public void CollectFields() {
        maxX = int.Parse(XBounds.text);
        maxY = int.Parse(YBounds.text);
        M = int.Parse(TerminalsCount.text);
        inititalPheromone = double.Parse(InitialIntensity.text);
        Maxloop = int.Parse(maxCycles.text);
        AntColony.rho = double.Parse(Rho.text);
        AntColony.Q = double.Parse(QValue.text);
        AntColony.alpha = double.Parse(alpha.text);
        AntColony.beta = double.Parse(beta.text);
        AntColony.gamma = double.Parse(gamma.text);
    }
    
    public void LoadFromFile() {
        StandaloneFileBrowser dialog = new StandaloneFileBrowser();
        ExtensionFilter[] filter = new ExtensionFilter[] {
            new ExtensionFilter("Текстовый файл","txt"),
            new ExtensionFilter("Все файлы","*")
        };
        var path = StandaloneFileBrowser.OpenFilePanel("Выбрать файл", "", filter, false);
        if (path.Length>0 && path[0]!="") {
            StreamReader reader = new StreamReader(path[0]);
            string result = reader.ReadToEnd();
            string[] lines = result.Split(new string[] { "\n" }, System.StringSplitOptions.RemoveEmptyEntries);
            vertices = new List<Vertex>();
            foreach (var line in lines) {
                string[] coords = line.Split(new string[] { " " }, System.StringSplitOptions.RemoveEmptyEntries);
                vertices.Add(new Vertex(double.Parse(coords[0]), double.Parse(coords[1]), true));
            }
            Create(false);
        }
    }

    public void Create(bool regenerate) {
        CollectFields();
        Destroy(graphParent.gameObject);
        graphParent = new GameObject("GraphParent").transform;
        Circles = new List<GameObject>();
        Crosses = new List<GameObject>();
        Edges = new Dictionary<Edge, GameObject>();
        if (regenerate)
            vertices = generate();
        grid = GraphGrid.buildGrid(vertices);
        foreach (var vert in grid.vertices) {
            if (vert != null) {
                if (vert.isTerminal) {
                    GameObject cross = Instantiate(Cross, new Vector3((float)vert.x, (float)vert.y, 0), Quaternion.identity, graphParent);
                    cross.name = string.Format("cross {0} {1}", vert.x, vert.y);
                    Crosses.Add(cross);
                }
                else {
                    GameObject circle = Instantiate(Circle, new Vector3((float)vert.x, (float)vert.y, 0), Quaternion.identity, graphParent);
                    circle.name = string.Format("circle {0} {1}", vert.x, vert.y);
                    Circles.Add(circle);
                }
            }
        }
        foreach (var edge in grid.edges) {
            GameObject newEdge = Instantiate(Edge, new Vector3((float)edge.start.x, (float)edge.start.y, 0), Quaternion.identity, graphParent);
            newEdge.transform.localScale = new Vector3((float)edge.length, 0.5f, 1);
            if (edge.start.x == edge.end.x) {
                newEdge.transform.eulerAngles = new Vector3(0, 0, edge.start.y < edge.end.y ? 90 : -90);
            }
            else {
                newEdge.transform.eulerAngles = new Vector3(0, 0, edge.start.x < edge.end.x ? 0 : 180);
            }
            newEdge.name = string.Format("Edge {0} {1}:{2} {3}", edge.start.x, edge.start.y, edge.end.x, edge.end.y);
            Edges.Add(edge, newEdge);
        }
        StopAllCoroutines();
        StartCoroutine(main());
    }

    private List<Vertex> generate() {
        List<Vertex> list = new List<Vertex>();
        for (int i = 0; i < M; i++) {
            int x = 0, y = 0;
            bool isIncluded = true;
            while (isIncluded) {
                x = Random.Range(-maxX / 5, maxX / 5) * 5;
                y = Random.Range(-maxY / 5, maxY / 5) * 5;
                bool isIn = false;
                foreach (var vert in list) {
                    if (vert.x == x && vert.y == y) {
                        isIn = true;
                    }
                }
                isIncluded = isIn;
            }
            list.Add(new Vertex(x, y, true));
        }
        return list;
    }

    private IEnumerator main() {
        timeText.text = "";
        yield return new WaitForEndOfFrame();
        foreach (var edge in grid.edges) {
            edge.intensity = inititalPheromone;
        }
        var colony = new AntColony(grid.terminals);
        bestLength = double.MaxValue;
        yield return new WaitForSeconds(1f);
        time = Time.time;
        foreach (var vert in Circles) {
            vert.SetActive(false);
        }
        foreach (var terminal in Crosses) {
            terminal.GetComponent<SpriteRenderer>().sprite = CircleFilled;
        }
        int count = 0;
        while (count < Maxloop) {
            Graph steinerTree = constructSteinerTree(colony, grid);
            double length = steinerTree.getLength();
            yield return new WaitForEndOfFrame();
            if (bestLength > length) {
                bestSteinerTree = steinerTree;
                DisplayTree(bestSteinerTree);
                bestLength = length;
                lengthtext.text = "Наименьш. длина " + bestLength;
            }
            foreach (var edge in grid.edges) {
                if (bestSteinerTree[edge.start, edge.end] != null)
                    edge.updateIntensity(bestLength);
                else
                    edge.intensity = ((double)1 - AntColony.rho) * edge.intensity;
            }
            foreach (var ant in colony.getAnts()) {
                ant.Revive();
            }
            count++;
            generation.text = string.Format("Поколение {0}/{1}", count, Maxloop);
        }
        DisplayTree(bestSteinerTree);
        time = Time.time - time;
        timeText.text = "Время " + time.ToString("0.##с");
        lengthtext.text = "Наименьш. длина " + bestSteinerTree.getLength();
        canvas.SetActive(true);
    }

    private void DisplayTree(Graph tree) {
        foreach (var e in Edges.Values) {
            e.transform.localScale = new Vector3(e.transform.localScale.x, 0.0f, e.transform.localScale.z);
        }
        foreach (var edge in tree.edges) {
            if (Edges.ContainsKey(edge)) {
                Edges[edge].transform.localScale = new Vector3(Edges[edge].transform.localScale.x, 1, Edges[edge].transform.localScale.z);
            }
        }
        
    }
    private Graph constructSteinerTree(AntColony colony, GraphGrid grid) {
        Graph steinerTree = new Graph();
        steinerTree.vertices = new List<Vertex>();
        steinerTree.edges = new List<Edge>();
        while (colony.antNumber > 0) {
            int m = Random.Range(0, colony.getAnts().Count);
            while (!colony.getAnts()[m].isAlive) {
                m = Random.Range(0, colony.getAnts().Count);
            }
            AntMoveResult result = colony.AntMove(colony.getAnts()[m], grid);
            while (!result.isMetAnt) {
                result = colony.AntMove(colony.getAnts()[m], grid);
                if (result.confused) {
                    colony.getAnts()[m].isAlive = false;
                    break;
                }
            }
            if (result.isMetAnt) {
                foreach (var vert in colony.getAnts()[m].tabuList) {
                    foreach (var met in result.metAnt)
                        if (!met.tabuList.Contains(vert))
                            met.tabuList.Add(vert);
                }
                foreach (var edge in colony.getAnts()[m].path) {
                    foreach (var met in result.metAnt)
                        if (!met.path.Contains(edge))
                            met.path.Add(edge);
                }
                colony.getAnts()[m].isAlive = false;
            }
        }
        foreach (Ant ant in colony.getAnts()) {
            foreach (Vertex vert in ant.tabuList) {
                if (!steinerTree.vertices.Contains(vert)) {
                    steinerTree.vertices.Add(vert);
                }
            }
            foreach (Edge edge in ant.path) {
                if (!steinerTree.edges.Contains(edge)) {
                    steinerTree.edges.Add(edge);
                }
            }
        }
        return steinerTree;
    }

}
