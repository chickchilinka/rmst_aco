﻿using System.Collections.Generic;
public class Ant
{
    public List<Vertex> tabuList;
    public Vertex currentVertex;
    public Vertex startVertex;
    public bool isAlive = true;
    public List<Edge> path;

    public Ant(Vertex startVertex) {
        this.startVertex = startVertex;
        this.currentVertex = startVertex;
        tabuList = new List<Vertex>();
        tabuList.Add(currentVertex);
        path = new List<Edge>();
    }
    public void Revive() {
        tabuList = new List<Vertex>();
        path = new List<Edge>();
        currentVertex = startVertex;
        tabuList.Add(currentVertex);
        isAlive = true;
    }
    public double getTourLength() {
        double sum = 0;
        foreach (var edge in path) {
            sum += edge.length;
        }
        return sum;
    }
}
